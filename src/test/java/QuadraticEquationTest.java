import equation.Equation;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by kucherenko on 07.03.15.
 */
public class QuadraticEquationTest {

    /**
     * test №1, 2, 3 for discriminant not equals zero
     * test №4 for discriminant equals zero
     */


    @Test
    public void testByDifferentValues() {

        //http://www.wolframalpha.com/input/?i=x+%5E+2+-+8+*+x+%2B+12+%3D+0

        double[] args = {1.0, -8.0, 12.0};
        String theExpResult = "2.0,6.0";
        Assert.assertEquals(theExpResult, Equation.solve(args));
    }

    @Test
    public void testByPositiveValues() {

        //http://www.wolframalpha.com/input/?i=x+%5E+2+%2B+8+*+x+%2B+12+%3D+0

        double[] args = {1.0, 8.0, 12.0};
        String theExpResult = "-6.0,-2.0";
        Assert.assertEquals(theExpResult, Equation.solve(args));
    }

    @Test
    public void testByNegativeValues() {

        //http://www.wolframalpha.com/input/?i=-x+%5E+2++-+8+*+x+-+12+%3D+0

        double[] args = {-1.0, -8.0, -12.0};
        String theExpResult = "-2.0,-6.0";
        Assert.assertEquals(theExpResult, Equation.solve(args));
    }

    @Test
    public void testByDiscriminantEqualsZero() {

        //http://www.wolframalpha.com/input/?i=x%5E2+-+6+*+x+%2B+9+%3D+0

        double[] args = {1.0, -6.0, 9.0};
        String theExpResult = "3.0";
        Assert.assertEquals(theExpResult, Equation.solve(args));
    }

}
