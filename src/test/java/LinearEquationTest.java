import equation.Equation;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by kucherenko on 07.03.15.
 */
public class LinearEquationTest {

    @Test
    public void testByPositiveValues() {
        double[] args = {4.0, 3.0};
        double expResult = - (4.0 / 3.0);
        String theExpResult = String.valueOf(expResult);
        Assert.assertEquals(theExpResult, Equation.solve(args));
    }

    @Test
    public void testByNegativeValues() {
        double[] args = {-4.0, -3.0};
        double expResult = - (-4.0 / -3.0);
        String theExpResult = String.valueOf(expResult);
        Assert.assertEquals(theExpResult, Equation.solve(args));
    }

    @Test
    public void testByNegativeMulValues() {
        double[] args = {4.0, -3.0};
        double expResult = - (4.0 / -3.0);
        String theExpResult = String.valueOf(expResult);
        Assert.assertEquals(theExpResult, Equation.solve(args));
    }

    @Test
    public void testByNegativeAddValues() {
        double[] args = {-4.0, 3.0};
        double expResult = - (-4.0 / 3.0);
        String theExpResult = String.valueOf(expResult);
        Assert.assertEquals(theExpResult, Equation.solve(args));
    }

    @Test
    public void testByBigValues() {
        double[] args = {2314.0, 1233.0};
        double expResult = - (2314.0 / 1233.0);
        String theExpResult = String.valueOf(expResult);
        Assert.assertEquals(theExpResult, Equation.solve(args));
    }

    @Test
    public void testBSmallValues() {
        double[] args = {0.1, 0.45};
        double expResult = - (0.1 / 0.45);
        String theExpResult = String.valueOf(expResult);
        Assert.assertEquals(theExpResult, Equation.solve(args));
    }

}
