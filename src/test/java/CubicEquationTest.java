import equation.Equation;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by kucherenko on 09.03.15.
 */
public class CubicEquationTest {

    @Test(expected = IllegalArgumentException.class)
    public void testCubicByFirstArgEqualsZero() {
        double[] args = {0.0, 3.0, 4.3, 2.4};
        Equation.solve(args);
    }

    @Test()
    public void testCubicByDLessZero() {

        double[] args = {1.0, 0.0, -7.0, 0.0};
        String expResult = "2.64,-5.6,-2.6";
        Assert.assertEquals(expResult, Equation.solve(args));

    }


    @Test()
    public void testCubicByDMoreZero() {

        //http://www.wolframalpha.com/input/?i=x+%5E3+%2B+0+*+x+%5E2+-+3+*+x+%2B+0+%3D+0

        double[] args = {1.0, 0.0, 3.0, 0.0};
        String expResult = "0.0";
        Assert.assertEquals(expResult, Equation.solve(args));

    }

    @Test()
    public void testCubicByDEqualsZero() {

        //http://www.wolframalpha.com/input/?i=x+%5E3+%2B+0+*+x+%5E2+-+0+*+x+%2B+0+%3D+0

        double[] args = {1.0, 0.0, 0.0, 0.0};
        String expResult = "0.0,0.0,0.0";
        Assert.assertEquals(expResult, Equation.solve(args));

    }

}
