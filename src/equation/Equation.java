package equation;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by kucherenko on 07.03.15.
 */
public class Equation {

    public static final int INDEX_OF_FIRST_ARG = 0;
    public static final int INDEX_OF_SECOND_ARG = 1;
    public static final int INDEX_OF_THIRD_ARG = 2;
    public static final int INDEX_OF_FOURTH_ARG = 3;

    public static final int ARGS_LENGTH_FOR_LINEAR = 2;
    public static final int ARGS_LENGTH_FOR_QUADRATIC = 3;
    public static final int ARGS_LENGTH_FOR_CUBIC = 4;

    public static String solve(double... args) {

        int argsLength = args.length;

        switch (argsLength) {

            case ARGS_LENGTH_FOR_LINEAR:

             return getLinearSolution(args);

            case ARGS_LENGTH_FOR_QUADRATIC:

            return getQuadraticSolution(args);

            case ARGS_LENGTH_FOR_CUBIC:

            return getCubicSolution(args);

        }

        return new String();
    }


    private static String getQuadraticSolution(double... args) {
        double firstArg  = args[INDEX_OF_FIRST_ARG];
        double secondArg = args[INDEX_OF_SECOND_ARG];
        double thirdArg  = args[INDEX_OF_THIRD_ARG];

        QuadraticEquation quadraticEquation = new QuadraticEquation(firstArg, secondArg, thirdArg);
        return quadraticEquation.getSolution();
    }

    private static String getCubicSolution(double... args) {

        double firstArg  = args[INDEX_OF_FIRST_ARG];
        double secondArg = args[INDEX_OF_SECOND_ARG];
        double thirdArg  = args[INDEX_OF_THIRD_ARG];
        double fourthArg  = args[INDEX_OF_FOURTH_ARG];

        CubicEquation cubicEquation = new CubicEquation(firstArg, secondArg, thirdArg, fourthArg);

        return cubicEquation.getSolution();
    }

    private static String getLinearSolution(double... args) {
        double addValue = args[INDEX_OF_FIRST_ARG];
        double mulValue = args[INDEX_OF_SECOND_ARG];

        LinearEquation linearEquation = new LinearEquation(addValue, mulValue);
        double solution = linearEquation.getSolution();
        return String.valueOf(solution);
    }


}
