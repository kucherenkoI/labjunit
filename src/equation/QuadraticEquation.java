package equation;

/**
 * Created by kucherenko on 07.03.15.
 */
public class QuadraticEquation {

    private static final double FOUR = 4.0;//1
    private static final double TWO = 2.0;//2

    private String solution;//3



    public QuadraticEquation(double firstValue, double secondValue, double thirdValue) {//4

        if(Math.sqrt(Math.pow(secondValue, TWO) - FOUR * firstValue * thirdValue) == 0){//5
			
            solution = String.valueOf(- secondValue / (TWO * firstValue));//6
            
        } else {//7
			
            double root1; //8
            double root2; //9
            root1 = (- secondValue + Math.sqrt(Math.pow(secondValue, TWO) - FOUR * firstValue * thirdValue)) / (TWO * firstValue); //10
            root2 = (- secondValue - Math.sqrt(Math.pow(secondValue, TWO) - FOUR * firstValue * thirdValue)) / (TWO * firstValue); //11
            solution = root2 + "," + root1; //12
        }
    }

    public String getSolution() { //13
        return solution; //14
    }


}
