package equation;

/**
 * Created by kucherenko on 07.03.15.
 */
public class LinearEquation extends Equation {

    private final double addValue;//1
    private final double mulValue;//2

    public LinearEquation(double addValue, double mulValue) { //3
        this.addValue = addValue;//4
        this.mulValue = mulValue;//5
    }
    public double getSolution() {//6
        return -(addValue / mulValue);//7
    }
}
