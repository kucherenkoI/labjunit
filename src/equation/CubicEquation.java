package equation;

/**
 * Created by kucherenko on 07.03.15.
 */
public class CubicEquation  {

    private static final double TWO_PI = 2.0 * Math.PI;//1
    private static final double FOUR_PI = 4.0 * Math.PI;//2

    private int nRoots;//3
    private double x1;//4
    private double x2;//5
    private double x3;//6

    public CubicEquation(double a,//7
                         double b,//8
                         double c,//9
                         double d) {//10

        if (a == 0.0) {//11
            throw new IllegalArgumentException("Cubic.solve(): a = 0");//12
        }

        double denom = a;//13
        a = b/denom;//14
        b = c/denom;//15
        c = d/denom;//16

        double a_over_3 = a / 3.0;//17
        double Q = (3*b - a*a) / 9.0;//18
        double Q_CUBE = Q*Q*Q;//19
        double R = (9*a*b - 27*c - 2*a*a*a) / 54.0;//20
        double R_SQR = R*R;//21
        double D = Q_CUBE + R_SQR;//22

        System.out.println(D);

        if (D < 0.0) {//23
            nRoots = 3;//24
            double theta = Math.acos (R / Math.sqrt (-Q_CUBE));//25
            double SQRT_Q = Math.sqrt (-Q);//26
            x1 = 2.0 * SQRT_Q * Math.cos (theta / 3.0) - a_over_3;//27
            x2 = 2.0 * SQRT_Q * Math.cos ((theta + TWO_PI) / 3.0) - a_over_3;//28
            x3 = 2.0 * SQRT_Q * Math.cos ((theta + FOUR_PI) / 3.0) - a_over_3;//29
            sortRoots();//30
        }
        else if (D > 0.0) {//31
            nRoots = 1;//32
            double SQRT_D = Math.sqrt (D);//33
            double S = Math.cbrt (R + SQRT_D);//34
            double T = Math.cbrt (R - SQRT_D);//35
            x1 = (S + T) - a_over_3;//36
            x2 = Double.NaN;//37
            x3 = Double.NaN;//38
        } else {
            nRoots = 3;//39
            double CBRT_R = Math.cbrt (R);//40
            x1 = 2*CBRT_R - a_over_3;//41
            x2 = x3 = CBRT_R - a_over_3;//42
            sortRoots();//43
        }
    }


    private void sortRoots() {//44
        if (x1 < x2) {//45
            double tmp = x1; x1 = x2; x2 = tmp;//46
        }
        if (x2 < x3) {//47
            double tmp = x2; x2 = x3; x3 = tmp;//48
        }
        if (x1 < x2) {//49
            double tmp = x1; x1 = x2; x2 = tmp;//50
        }
    }

    /**
     *
     * Такая нумерация потому что сначала сделал граф, а потом внёс изменения в код.
     */

    public String getSolution() {//51
        final String coma = ",";//52

        final int maxRootLength = 4;
        final int indexOfFirstChar = 0;

        String solution = "";
        String root1 = String.valueOf(x1);
        if(root1.length() < maxRootLength) {
            solution = root1.substring(indexOfFirstChar, root1.length());
        } else {
            solution = root1.substring(indexOfFirstChar, maxRootLength);//53
        }
        final int three = 3;//54
        if(nRoots == three) {//55

            String root2 = String.valueOf(x2);
            if(root2.length() < maxRootLength) {
                solution += coma + root2.substring(indexOfFirstChar, root2.length());
            } else {
                solution += coma + root2.substring(indexOfFirstChar, maxRootLength);//53
            }

            String root3 = String.valueOf(x3);
            if(root3.length() < maxRootLength) {
                solution += coma + root3.substring(indexOfFirstChar, root2.length());
            } else {
                solution += coma + root3.substring(indexOfFirstChar, maxRootLength);//53
            }
        }
        return solution;//57
    }


}
